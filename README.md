# Gradle Url2Maven Plugin
[![pipeline status](https://gitlab.com/jkushmaul/gradle-url2maven-plugin/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/gradle-url2maven-plugin/commits/master)

Download a file via URL to maven local dependency cache (for tool execution, configuration, etc)

# How
In your build.gradle:  
```
apply plugin: "io.gitlab.jkushmaul.url2maven"

dependencies {
    url2maven("SomeGroup:SomeArtifact:2.3.4@zip") {
        ext.url = "https://bitbucket.org/SomeGroup/SomeArtifact/downloads/SomeArtifact-0.05.zip"
        ext.sha1 = "fa408d98c729486d343a8b55317624370663859a"
    }
    url2maven("SomeGroup:SomeArtifact:2.3.4@jar") {
        ext.url = "https://bitbucket.org/SomeGroup/SomeArtifact/downloads/SomeArtifact-2.3.4.jar"
        ext.sha1 = "37b4ddf1c6a63280a351c1c6053fb771362c7f20"
    }
}
```

You can then use your dependency by finding the resolvedArtifact like below:
```
File getToolPath(Project project) {
        ResolvedArtifact artifact = project.configurations.url2maven.resolvedConfiguration.resolvedArtifacts.find {
            it.name == "apktool"
        }
        if (artifact == null) {
            throw new GradleException("apktool dependency was not found: are we in afterEvaluate?")
        }
        artifact.file
    }
task someTask {
    doLast {
      def mytool = getToolPath(project)
      project.exec {
                workingDir buildDir
                commandLine  "${mytool}", "arg1", "arg2"
                standardOutput = stdout
                errorOutput = stdout
            }
    }
}
```


# What
1. This first adds mavenLocal and buildDir/flatDir repos
1. It adds placeholder files of zero bytes in buildDir/flatDir
1. When mavenLocal does not have the dep, it will resolve in the buildDir/flatDir
1. Runtime will check for the flatDir path as the resolved path
1. If flatDir, it will download the URL into buildDir
1. It then maven publishes to local and updates the dependency path.
1. Future builds just bypass all of this because mavenLocal resolves.



# Why

https://github.com/gradle/gradle/issues/5322  

## The need was simple  
* I needed to download a Jar as an execution dependency
* I wanted to have gradle cache the dependency 

## It was difficult
* But maven, ivy?  You can customize maven/ivy to craft the URL given some macros
* Kind of works - except it's a PITA for what I want to do that is so simple.
* Oh, and it does not work with anything that does not support standard HEAD
  requests, like S3 (bitbucket)
  
## ugly, if not completely unnecessary
There's likely a way to do this with the existing maven url customization
which can disable the ned for the HEAD request, but that's not clear to me and
this was much more entertaining.    
 
I'm not a groovy dev and it will show; there are likely some optimizations to be had, or even some very unnecessary tactics
I took to get there.

Normally, you can just use patternLayout:
```
repositories {
    ivy {
        url "https://bitbucket.org"

        patternLayout {
            artifact "[organization]/[module]/downloads/[artifact]-[revision].[ext]"
            //does work because HTTP HEAD request... :/
        }
        content {
            includeGroup "MyGroup"
        }
    }
}

dependencies {
    smalidea("MyGroup:MyArtifact:0.05@zip")
}
```
I think once the gradle gods address  
https://github.com/gradle/gradle/issues/5322  
Then this will be unnecessary until the next time.